mkdir chapter-1
touch chapter-1/README.md
touch chapter-1/docker_installation.md
touch chapter-1/docker_compose_installation.md

mkdir chapter-2
touch chapter-2/README.md
touch chapter-2/hello_world.md
touch chapter-2/crud_containers.md

mkdir chapter-3
touch chapter-3/README.md
touch chapter-3/mysql.md
touch chapter-3/multiple_mysql.md
touch chapter-3/persistence.md

mkdir chapter-4
touch chapter-4/README.md
touch chapter-4/dockerfile.md
touch chapter-4/crud_images.md

mkdir chapter-5
touch chapter-5/README.md
touch chapter-5/wordpress.md
